﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Media;

namespace CAALPHUILib
{
    /// <summary>
    /// Interaction logic for CSAlertScreen.xaml
    /// </summary>
    public partial class CSAlertScreen : UserControl
    {
        public CSAlertScreen()
        {
            InitializeComponent();

            DataContext = this;

        }


        static FrameworkPropertyMetadata propertymetadataAlertMainTitle =
            new FrameworkPropertyMetadata("",
            FrameworkPropertyMetadataOptions.BindsTwoWayByDefault |
            FrameworkPropertyMetadataOptions.Journal, new
            PropertyChangedCallback(AlertMainTitle_PropertyChanged),
            new CoerceValueCallback(AlertMainTitle_CoerceValue),
            false, UpdateSourceTrigger.PropertyChanged);

        static FrameworkPropertyMetadata propertymetadataAlertSubTitle =
            new FrameworkPropertyMetadata("",
            FrameworkPropertyMetadataOptions.BindsTwoWayByDefault |
            FrameworkPropertyMetadataOptions.Journal, new
            PropertyChangedCallback(AlertSubTitle_PropertyChanged),
            new CoerceValueCallback(AlertSubTitle_CoerceValue),
            false, UpdateSourceTrigger.PropertyChanged);

        static FrameworkPropertyMetadata propertymetadataAlertTextMessage =
            new FrameworkPropertyMetadata("",
            FrameworkPropertyMetadataOptions.BindsTwoWayByDefault |
            FrameworkPropertyMetadataOptions.Journal, new
            PropertyChangedCallback(AlertTextMessage_PropertyChanged),
            new CoerceValueCallback(AlertTextMessage_CoerceValue),
            false, UpdateSourceTrigger.PropertyChanged);

        public static readonly DependencyProperty AlertMainTitleProperty = DependencyProperty.Register("AlertMainTitle", typeof(String), typeof(CSTile), propertymetadataAlertMainTitle);
        public static readonly DependencyProperty AlertSubTitleProperty = DependencyProperty.Register("AlertSubTitle", typeof(String), typeof(CSTile), propertymetadataAlertSubTitle);
        public static readonly DependencyProperty AlertTextMessageProperty = DependencyProperty.Register("AlertTextMessage", typeof(String), typeof(CSTile), propertymetadataAlertTextMessage);

        private static void AlertMainTitle_PropertyChanged(DependencyObject dobj, DependencyPropertyChangedEventArgs e)
        {
        }
        private static void AlertSubTitle_PropertyChanged(DependencyObject dobj, DependencyPropertyChangedEventArgs e)
        {
        }
        private static void AlertTextMessage_PropertyChanged(DependencyObject dobj, DependencyPropertyChangedEventArgs e)
        {
        }

        private static object AlertMainTitle_CoerceValue(DependencyObject dobj, object Value)
        {
            return Value;
        }
        private static object AlertSubTitle_CoerceValue(DependencyObject dobj, object Value)
        {
            return Value;
        }
        private static object AlertTextMessage_CoerceValue(DependencyObject dobj, object Value)
        {
            return Value;
        }

        public String AlertMainTitle
        {
            get
            {
                return (String)this.GetValue(AlertMainTitleProperty);
            }
            set
            {
                this.SetValue(AlertMainTitleProperty, value);
            }
        }
        public static void SetAlertMainTitle(DependencyObject dependencyObject, String value)
        {
            dependencyObject.SetValue(AlertMainTitleProperty, value);
        }
        public static String GetAlertMainTitle(DependencyObject dependencyObject)
        {
            return (String)dependencyObject.GetValue(AlertMainTitleProperty);
        }

        public String AlertSubTitle
        {
            get
            {
                return (String)this.GetValue(AlertSubTitleProperty);
            }
            set
            {
                this.SetValue(AlertSubTitleProperty, value);
            }
        }
        public static void SetAlertSubTitle(DependencyObject dependencyObject, String value)
        {
            dependencyObject.SetValue(AlertSubTitleProperty, value);
        }
        public static String GetAlertSubTitle(DependencyObject dependencyObject)
        {
            return (String)dependencyObject.GetValue(AlertSubTitleProperty);
        }

        public String AlertTextMessage
        {
            get
            {
                return (String)this.GetValue(AlertTextMessageProperty);
            }
            set
            {
                this.SetValue(AlertTextMessageProperty, value);
            }
        }
        public static void SetAlertTextMessage(DependencyObject dependencyObject, String value)
        {
            dependencyObject.SetValue(AlertTextMessageProperty, value);
        }
        public static String GetAlertTextMessage(DependencyObject dependencyObject)
        {
            return (String)dependencyObject.GetValue(AlertTextMessageProperty);
        }

        private void Canvas_MouseEnter(object sender, MouseEventArgs e)
        {
            var bc = new BrushConverter();
            cancelBtn.Background = (Brush)bc.ConvertFrom("#FF00d5ff");
        }

        private void Canvas_MouseLeave(object sender, MouseEventArgs e)
        {
            var bc = new BrushConverter();
            cancelBtn.Background = (Brush)bc.ConvertFrom("#FF7EDAEC");
        }

        private void Canvas_MouseDown(object sender, MouseButtonEventArgs e)
        {
            this.Visibility = Visibility.Collapsed;
        }

        private void confirmBtn_MouseEnter(object sender, MouseEventArgs e)
        {
            var bc = new BrushConverter();
            confirmBtn.Background = (Brush)bc.ConvertFrom("#FF00d5ff");
        }

        private void confirmBtn_MouseLeave(object sender, MouseEventArgs e)
        {
            var bc = new BrushConverter();
            confirmBtn.Background = (Brush)bc.ConvertFrom("#FF7EDAEC");
        }





    }
}
