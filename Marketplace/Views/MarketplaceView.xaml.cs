﻿using GalaSoft.MvvmLight.Messaging;
using GalaSoft.MvvmLight.Threading;

namespace Marketplace.Views
{
    /// <summary>
    /// Interaction logic for MarketplaceView.xaml
    /// </summary>
    public partial class MarketplaceView
    {
        public MarketplaceView()
        {
            InitializeComponent();
            Messenger.Default.Register<NotificationMessage>(this, ShowView);
        }

        private void ShowView(NotificationMessage note)
        {
            if (note.Target.Equals(GetType()) && note.Notification.Equals("Show"))
            {
                DispatcherHelper.CheckBeginInvokeOnUI(Show);
                DispatcherHelper.CheckBeginInvokeOnUI(BringToFront);
            }
        }

        private void BringToFront()
        {
            Activate();
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            e.Cancel = true;
            Hide();
        }
    }
}
