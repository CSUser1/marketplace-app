﻿using System;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using MarketplaceContract;
using Newtonsoft.Json;

namespace Marketplace.Model
{
    public class AppProfileDataService : AppDataServiceBase, IAppProfileDataService
    {
        public Task<ApplicationProfile> LoadAppProfile(int id, CancellationToken cancellationToken)
        {
            return LoadAppProfileAsync(id, cancellationToken);
        }

        public async Task<ApplicationProfile> LoadAppProfileAsync(int id, CancellationToken cancellationToken)
        {
            return await Task.Run(() => GetAppProfile(id), cancellationToken);
        }

        private ApplicationProfile GetAppProfile(int id)
        {
            //TODO: get this url from config
            const string relativeUri = "api/applications/";
            try
            {
                var webClient = new CookieWebClient();
                AuthorizeCaalhp(webClient);
                //var webClient = new WebClient();
                ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };
                var uri = new Uri(BaseAddress, relativeUri + id);
                var content = webClient.DownloadString(uri);
                var result = JsonConvert.DeserializeObject<ApplicationProfile>(content);
                return result;
            }
            catch (Exception exception)
            {
                return new ApplicationProfile();
            }
        }
    }
}