﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using MarketplaceContract;

namespace Marketplace.Model
{
    public interface IAppDataService
    {
        Task<IList<Application>> LoadApps(CancellationToken cancellationToken);
        Task<IList<Application>> LoadAppsAsync(CancellationToken cancellationToken);
    }
}

