﻿using System.Threading;
using System.Threading.Tasks;
using MarketplaceContract;

namespace Marketplace.Model
{
    public interface IAppProfileDataService
    {
        Task<ApplicationProfile> LoadAppProfile(int id, CancellationToken cancellationToken);
        Task<ApplicationProfile> LoadAppProfileAsync(int id, CancellationToken cancellationToken);
    }
}