﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Windows.Threading;
using caalhp.Core.Contracts;
using caalhp.Core.Events;
using caalhp.Core.Utils.Helpers;
using caalhp.Core.Utils.Helpers.Serialization;
using caalhp.Core.Utils.Helpers.Serialization.JSON;
using caalhp.IcePluginAdapters.WPF;
using GalaSoft.MvvmLight.Messaging;
using Marketplace.ViewModel;
using MarketplaceContract;

namespace Marketplace
{
    public class MarketplaceImplementation : IAppCAALHPContract
    {
        private IAppHostCAALHPContract _host;
        private int _processId;
        private readonly MarketplaceViewModel _marketplace;

        public MarketplaceImplementation(MarketplaceViewModel marketplace)
        {
            _marketplace = marketplace;
            Messenger.Default.Register<NotificationMessage<Application>>(this, SendDownloadRequest);
        }

        private void SendDownloadRequest(NotificationMessage<Application> notificationMessage)
        {
            if (!notificationMessage.Target.Equals(GetType())) return;

            var downloadAppEvent = new DownloadAppEvent
            {
                CallerName = GetName(),
                CallerProcessId = _processId,
                AppName = notificationMessage.Content.Name,
                FileName = notificationMessage.Content.Url
            };
            var serializedEvent = EventHelper.CreateEvent(SerializationType.Json, downloadAppEvent);
            _host.Host.ReportEvent(serializedEvent);
        }

        public void Notify(KeyValuePair<string, string> notification)
        {
            var type = EventHelper.GetTypeFromFullyQualifiedNameSpace(notification.Key);
            dynamic obj = JsonSerializer.DeserializeEvent(notification.Value, type);
            HandleEvent(obj);
        }

        private void HandleEvent(ShowAppEvent showAppEvent)
        {
            if (showAppEvent.AppName.Equals(GetName()))
                Show();
        }

        private void HandleEvent(DownloadAppCompletedEvent downloadAppCompletedEvent)
        {
            Messenger.Default.Send(downloadAppCompletedEvent);//Send(new NotificationMessage<DownloadAppCompletedEvent>();
        }

        private void HandleEvent(DownloadProgressEvent downloadProgressEvent)
        {
            //var progress = downloadProgressEvent.PercentDone;
            //var fileName = downloadProgressEvent.FileName;
            Messenger.Default.Send(downloadProgressEvent);
                //new NotificationMessage(this, typeof(AppItemViewModel), "DownloadProgress" + ";" + fileName + ";" + progress));
        }

        private void HandleEvent(InstallAppCompletedEvent installAppCompletedEvent)
        {
            Messenger.Default.Send(installAppCompletedEvent);//new NotificationMessage(this, typeof(AppItemViewModel), installAppCompletedEvent.FileName + ";" + "InstallCompleted"));
        }

        private void HandleEvent(InstallAppFailedEvent installAppFailedEvent)
        {
            Messenger.Default.Send(installAppFailedEvent);//new NotificationMessage(this, typeof(AppItemViewModel), installAppCompletedEvent.FileName + ";" + "InstallCompleted"));
        }

        public string GetName()
        {
            return "Marketplace";
        }

        public bool IsAlive()
        {
            return true;
        }

        public void ShutDown()
        {
            System.Windows.Application.Current.Dispatcher.BeginInvokeShutdown(DispatcherPriority.Normal);
        }

        public void Initialize(IAppHostCAALHPContract hostObj, int processId)
        {
            _host = hostObj;
            _processId = processId;
            //Subscribe to events so we can react to "Home" messages
            _host.Host.SubscribeToEvents(EventHelper.GetFullyQualifiedNameSpace(SerializationType.Json, typeof(ShowAppEvent)), _processId);
            _host.Host.SubscribeToEvents(EventHelper.GetFullyQualifiedNameSpace(SerializationType.Json, typeof(DownloadAppCompletedEvent)), _processId);
            _host.Host.SubscribeToEvents(EventHelper.GetFullyQualifiedNameSpace(SerializationType.Json, typeof(DownloadProgressEvent)), _processId);
            _host.Host.SubscribeToEvents(EventHelper.GetFullyQualifiedNameSpace(SerializationType.Json, typeof(InstallAppCompletedEvent)), _processId);
            _host.Host.SubscribeToEvents(EventHelper.GetFullyQualifiedNameSpace(SerializationType.Json, typeof(InstallAppFailedEvent)), _processId);
        }

        public void Show()
        {
            DispatchToApp(Helper.BringToFront);

            //ref: http://reedcopsey.com/2011/11/28/launching-a-wpf-window-in-a-separate-thread-part-1/
            //Threading is used so we do not block the main thread.
            _marketplace.InstalledApps.Clear();
            foreach (var app in _host.GetListOfInstalledApps())
            {
                _marketplace.InstalledApps.Add(Path.GetFileName(app.LocationDir));
            }
            _marketplace.Show();

        }

        private void DispatchToApp(Action action)
        {
            System.Windows.Application.Current.Dispatcher.Invoke(action);
        }
    }
}